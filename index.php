<?php
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'product.php';
include_once 'category.php';
 
$database = new Database();
$db = $database->getConnection();
 
$product = new Product($db);
$category = new Category($db);
 
$page_title = "Product List";
include_once "layouts/header.php";
 
// query products
$stmt = $product->readAll($from_record_num, $records_per_page);
 
// specify the page where paging is used
$page_url = "index.php?";
 
// count total rows - used for pagination
$total_rows=$product->countAll();
 
// read_template.php controls how the product list will be rendered
include_once "product_list.php";
 
// layout_footer.php holds our javascript and closing html tags
include_once "layouts/footer.php";
?>