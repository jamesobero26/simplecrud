<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../config/database.php';
include_once '../../product.php';

$instance = Database::getInstance();
$conn = $instance->getConnection();

$products = new Product($conn);

$stmt = $products->read();
$num = $stmt->rowCount();

if ($num > 0) {
	$products_arr = array();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		array_push($products_arr, $row);
	}

	http_response_code(200);
	echo json_encode(array('records' => $products_arr));
} else {
	http_response_code(404);
	echo json_encode([
		'message' => 'No products found.',
	]);
}

?>