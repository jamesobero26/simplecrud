<?php
class Database{
  
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "simplecrud";
    private $username = "root";
    private $password = "";
    
    private static $instance  = null;
    private $conn = null;
  
    private function __construct() {
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

    // get the database connection
    public function getConnection(){
        return $this->conn;
    }
}
?>