<?php 
function Palindrome($string){   
    if (strrev($string) == $string){   
        return 1;   
    } 
    else{ 
        return 0; 
    } 
}   
  
// Driver Code 
$original = "123454321";  
if(Palindrome($original)){   
    echo "Palindrome";   
}  
else {   
echo "Not a Palindrome";   
}

function PalindromeNumer($number){   
    $temp = $number;   
    $new = 0;   
    while (floor($temp)) {   
        $d = $temp % 10;   
        $new = $new * 10 + $d;   
        $temp = $temp/10;   
    }   
    if ($new == $number){   
        return 1;   
    } 
    else{ 
        return 0; 
    } 
}   
  
// Driver Code 
$original = 1441;  
if (PalindromeNumer($original)){   
    echo "Palindrome";   
}  
else {   
echo "Not a Palindrome";   
} 
  
?> 