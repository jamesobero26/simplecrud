CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- Dumping data for table `products`
INSERT INTO `products` (`id`, `name`, `description`, `price`, `category_id`, `created`, `modified`) VALUES
(1, 'LG P880 4X HD', 'My first awesome phone!', 336, 3, 'now()', 'now()'),
(2, 'Google Nexus 4', 'The most awesome phone of 2013!', 299, 2, 'now()', 'now()'),
(3, 'Samsung Galaxy S4', 'How about no?', 600, 3, 'now()', 'now()'),
(6, 'Bench Shirt', 'The best shirt!', 29, 1, 'now()', 'now()'),
(7, 'Lenovo Laptop', 'My business partner.', 399, 2, 'now()', 'now()'),
(8, 'Samsung Galaxy Tab 10.1', 'Good tablet.', 259, 2, 'now()', 'now()'),
(9, 'Spalding Watch', 'My sports watch.', 199, 1, 'now()', 'now()'),
(10, 'Sony Smart Watch', 'The coolest smart watch!', 300, 2, 'now()', 'now()'),
(11, 'Huawei Y300', 'For testing purposes.', 100, 2, 'now()', 'now()'),
(12, 'Abercrombie Lake Arnold Shirt', 'Perfect as gift!', 60, 1, 'now()', 'now()'),
(13, 'Abercrombie Allen Brook Shirt', 'Cool red shirt!', 70, 1, 'now()', 'now()'),
(25, 'Abercrombie Allen Anew Shirt', 'Awesome new shirt!', 999, 1, 'now()', 'now()'),
(26, 'Another product', 'Awesome product!', 555, 2, 'now()', 'now()'),
(27, 'Bag', 'Awesome bag for you!', 999, 1, 'now()', 'now()'),
(28, 'Wallet', 'You can absolutely use this one!', 799, 1, 'now()', 'now()'),
(30, 'Wal-mart Shirt', '', 555, 2, 'now()', 'now()'),
(31, 'Amanda Waller Shirt', 'New awesome shirt!', 333, 1, 'now()', 'now()'),
(32, 'Washing Machine Model PTRR', 'Some new product.', 999, 1, 'now()', 'now()');


CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- Dumping data for table `categories`
INSERT INTO `categories` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Fashion', 'now()', 'now()'),
(2, 'Electronics', 'now()', 'now()'),
(3, 'Motors', 'now()', 'now()');
